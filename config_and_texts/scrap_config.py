NB_DOMAINS_TO_CHECK = 100
MINIMAL_SATISFYING_NB_OF_RESULTS = 20

QUERIES_FILE_SLUG = "queries.txt"


WEBSITES_TO_AVOID = [
    "amazon",
    "nationalgeographic",
    "wiktionary",
    "cdiscount",
    "synonymo",
    "booking",
    "xbox.com",
    "vikidia",
    "gettyimages",
    "shutterstock",
    "babelio",
    "etsy",
    "wiki",
    "youtube",
    "facebook",
    "pinterest",
    "instagram",
    "twitter",
    "reverso",
    "Larousse",
    "Cloudfare",
    "403",
    "ERROR",
    "Firewall",
    "Denied",
    "dictio",
    "quora",
    "reddit",
    "walmart",
    "yahoo",
    "linternaute",
    "femmeactuelle" "cultura",
    "fnac",
    "pandora",
    "histoiredor",
    "microsoft",
    "apple",
    "medium",
    "linkedin",
    "francetvinfo",
    "lefigaro",
    "lemonde",
    "over-blog",
    "sciencenet.cn",
    "t.me",
]

SUBSTRINGS_TO_EXCLUDE_IN_MAILS = [
    "png",
    "jpg",
    "prestashop",
    "gif",
    ".js",
    ".png",
    "2x.png",
    "jpeg",
    "webp",
    "wix",
    "svg",
    ".io",
    "example.com",
    "yourname" "gov.",
    "avif",
    "noreply",
    "exemple",
    "example",
    "google",
    "spam",
    "exemple",
    "report",
    "abuse",
]


LTD_DICT = {
    "en": "com",
    "pt": "br",
    "sv": "se",
    "zh": "cn",
    "ja": "jp",
    "ko": "kr",
    "da": "dk",
    "ct": "cat",
}

GOOD_LANG_DICT = {
    "sv": "se",
    "zh": "cn",
    "ja": "jp",
    "ko": "kr",
    "da": "dk",
    "ct": "cat",
}


def get_ltd_from_lang(lang):
    if lang in LTD_DICT:
        return LTD_DICT[lang]
    else:
        return lang


def get_good_lang(lang):
    if lang in GOOD_LANG_DICT:
        return GOOD_LANG_DICT[lang]
    else:
        return lang
