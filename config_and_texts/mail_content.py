import os, sys

submodules_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "submodules"))
sys.path.append(submodules_path)

from submodules.python_helpers.strings_magnifier import replace_multiple_regex_in_string


MAIL_TITLE = {
    "en": "Partnership Proposal (and thanks for your work!)",
    "ko": "협력 제안 (그리고 여러분의 작품에 대한 감사!)",
    "lt": "Partnerystės pasiūlymas (ir ačiū už jūsų darbą!)",
    "es": "Propuesta de colaboración (¡y agradecimiento por su trabajo!)",
    "ru": "Предложение о партнерстве (и благодарность за вашу работу!)",
    "lv": "Partnerības priekšlikums (un paldies par jūsu darbu!)",
    "fr": "Proposition de partenariat (et remerciement pour votre travail ! )",
    "uk": "Пропозиція про партнерство (і дякую за вашу роботу!)",  # FIXME 2 ?
    "hu": "Partnerségi javaslat (és köszönöm a munkáját!)",
    "de": "Partnerschaftsvorschlag (und Dank für Ihre Arbeit!)",
    "tr": "Ortaklık Teklifi (ve çalışmanız için teşekkürler!)",
    "no": "Forslag om partnerskap (og takk for arbeidet ditt!)",
    "pt": "Proposta de parceria (e agradecimento pelo seu trabalho!)",
    "af": "Vennootskapsvoorstel (en dankie vir jou werk!)",  # FIXME 3 ?
    "pl": "Propozycja współpracy (i podziękowanie za Twoją pracę!)",
    "it": "Proposta di partenariato (e ringraziamento per il tuo lavoro!)",
    "id": "Proposal Kemitraan (dan terima kasih atas pekerjaan Anda!)",
    "ro": "Propunere de parteneriat (și mulțumiri pentru munca ta!)",
    "nl": "Voorstel tot samenwerking (en dank voor uw werk!)",
    "ca": "Proposta d'associació (i gràcies per la vostra feina!)",  # FIXME 1 ?
    "sk": "Návrh partnerstva (a vďaka za vašu prácu!)",
    "zh": "合作提议（并感谢您的工作！）",
    "da": "Forslag om partnerskab (og tak for dit arbejde!)",
    "fi": "Kumppanuusehdotus (ja kiitos työstäsi!)",
    "ja": "提携の提案（そしてあなたの仕事に感謝！）",
    "hr": "Prijedlog partnerstva (i hvala na vašem radu!)",
    "sv": "Förslag till samarbete (och tack för ert arbete!)",
    "ct": "Proposta d'associació (i gràcies per la vostra feina!)",  # FIXME 1 ?
    "ua": "Пропозиція про партнерство (і дякую за вашу роботу!)",  # FIXME 2 ?
    "za": "Vennootskapsvoorstel (en dankie vir jou werk!)",  # FIXME 3 ?
}


MAIL_BODY = {
    "en": """Hello!

First of all, I would like to thank you for your work that I discovered a few months ago and found more than interesting. Your vision of $$$, but especially the way you talk about it, appealed to me.

If I am contacting you today, it's because I am working on a platform with a fairly similar theme. ($$$)

In short, I think a collaboration could benefit both of our sites. I have a proposal for you: the writing of a guest article (I will write an article for you of about 1500 words, optimized for a keyword of your choice, not generated with IA and with a deep SEO work).

In any case, thanks again for your work, and your response (whatever it may be) is appreciated.

Cyril Gendarme, for the "$$$" team""",
    "ko": """안녕하세요!

먼저, 몇 달 전에 발견한 귀하의 작업에 대해 감사의 말씀을 전하고 싶습니다. $$$에 대한 귀하의 비전, 특히 그것을 설명하는 방식이 저에게 깊은 인상을 주었습니다.

오늘 이렇게 연락을 드리는 이유는 제가 비슷한 주제를 다루는 플랫폼을 작업하고 있기 때문입니다. ($$$)

간단히 말해서, 협업이 우리 사이트 모두에게 도움이 될 수 있다고 생각합니다. 다음과 같은 제안을 드리고 싶습니다: 게스트 기고문 작성 (제가 약 1500단어 분량의 기사로, 선택한 키워드에 최적화된, IA로 생성되지 않은, 심도 있는 SEO 작업을 포함한 기사를 작성하겠습니다).

어쨌든, 귀하의 작업에 다시 한번 감사드리며, 귀하의 답변(어떤 내용이든)을 소중히 여기겠습니다.

Cyril Gendarme, "$$$" 팀을 대표하여""",
    "lt": """Sveiki!

Visų pirma, norėčiau padėkoti už jūsų darbą, kurį atradau prieš kelis mėnesius ir kuris mane labai sužavėjo. Jūsų požiūris į $$$, o ypač būdas, kuriuo apie tai kalbate, man labai patiko.

Kreipiuosi į jus šiandien, nes dirbu su platforma, kuri yra gana panaši tema. ($$$)

Trumpai tariant, manau, kad bendradarbiavimas būtų naudingas abiem mūsų svetainėms. Turiu jums pasiūlymą: svečio straipsnio parašymas (aš parašysiu jums apie 1500 žodžių straipsnį, optimizuotą pagal jūsų pasirinktą raktinį žodį, nesukurtą naudojant IA ir atliekant gilų SEO darbą).

Bet kuriuo atveju, dar kartą dėkoju už jūsų darbą, ir jūsų atsakymas (koks jis bebūtų) bus vertinamas.

Cyril Gendarme, už "$$$" komandą""",
    "es": '''¡Hola!

En primer lugar, me gustaría agradecerte por tu trabajo, que descubrí hace unos meses y me pareció más que interesante. Tu visión sobre $$$, y especialmente la manera en que hablas de ello, me encantó.

Si te contacto hoy, es porque estoy trabajando en una plataforma con una temática bastante similar. ($$$)

En resumen, creo que una colaboración podría beneficiar a ambos sitios. Tengo una propuesta para ti: la redacción de un artículo como invitado (escribiré un artículo para ti de unas 1500 palabras, optimizado para una palabra clave de tu elección, no generado con IA y con un profundo trabajo de SEO).

De todas formas, gracias nuevamente por tu trabajo, y tu respuesta (sea cual sea) será muy apreciada.

Cyril Gendarme, para el equipo de "$$$"''',
    "ru": '''Здравствуйте!

Прежде всего, хочу поблагодарить вас за вашу работу, которую я обнаружил несколько месяцев назад и нашел более чем интересной. Ваш взгляд на $$$, а особенно то, как вы об этом рассказываете, привлек мое внимание.

Я связываюсь с вами сегодня, потому что работаю над платформой с довольно схожей тематикой. ($$$)

Вкратце, я думаю, что сотрудничество может быть выгодно для обоих наших сайтов. У меня есть предложение для вас: написание гостевой статьи (я напишу для вас статью объемом около 1500 слов, оптимизированную под ключевое слово на ваш выбор, не созданную с помощью ИИ, с глубокой проработкой SEO).

В любом случае, еще раз спасибо за вашу работу, и ваш ответ (каким бы он ни был) будет для меня ценным.

С уважением,
Cyril Gendarme, от команды "$$$"''',
    "lv": """Sveiki!

Vispirms es vēlētos pateikties par jūsu darbu, kuru atklāju pirms dažiem mēnešiem un atradu ļoti interesantu. Jūsu redzējums par $$$, bet jo īpaši tas, kā jūs par to runājat, mani ļoti uzrunāja.

Šodien es sazinājos ar jums, jo strādāju pie platformas ar diezgan līdzīgu tēmu. ($$$)

Īsumā, es domāju, ka sadarbība varētu būt izdevīga abām mūsu vietnēm. Man ir piedāvājums jums: viesraksta rakstīšana (es uzrakstīšu rakstu jums, aptuveni 1500 vārdus garu, optimizētu pēc jūsu izvēlēta atslēgvārda, neradītu ar mākslīgo intelektu un ar padziļinātu SEO darbu).

Jebkurā gadījumā, paldies vēlreiz par jūsu darbu, un jūsu atbilde (kāda tā arī būtu) būs ļoti novērtēta.

Cyril Gendarme, no "$$$" komandas""",
    "fr": '''Bonjour !

Tout d'abord, je tiens à vous remercier pour votre travail que j'ai découvert il y a quelques mois et que j'ai trouvé plus qu'intéressant. Votre vision de $$$, mais surtout la manière dont vous en parlez, m'a séduit.

Si je vous contacte aujourd'hui, c'est parce que je travaille sur une plateforme ayant un thème assez similaire. ($$$)

En résumé, je pense qu'une collaboration pourrait être bénéfique pour nos deux sites. J'ai une proposition pour vous : la rédaction d'un article invité (je rédigerai un article pour vous d'environ 1500 mots, optimisé pour un mot-clé de votre choix, non généré avec IA et avec un travail SEO approfondi).

En tout cas, merci encore pour votre travail, et votre réponse (quelle qu'elle soit) sera appréciée.

Cyril Gendarme, pour l'équipe de "$$$"''',
    "uk": '''Вітаю!

Перш за все, я хотів би подякувати вам за вашу роботу, яку я відкрив кілька місяців тому і яка виявилася надзвичайно цікавою. Ваша бачення $$$, а особливо те, як ви про це говорите, мене вразило.

Сьогодні я звертаюсь до вас, бо працюю над платформою з досить схожою тематикою. ($$$)

Коротко кажучи, я думаю, що співпраця може бути корисною для обох наших сайтів. У мене є пропозиція для вас: написання гостьової статті (я напишу для вас статтю приблизно на 1500 слів, оптимізовану під ключове слово на ваш вибір, не створену за допомогою ШІ, і з глибокою SEO-оптимізацією).

У будь-якому разі, ще раз дякую за вашу роботу, і ваша відповідь (якою б вона не була) буде дуже цінною.

З повагою,
Cyril Gendarme, від команди "$$$"''',
    "hu": """Helló!

Először is szeretném megköszönni a munkáját, amelyet néhány hónapja fedeztem fel, és rendkívül érdekesnek találtam. Az Ön nézőpontja a $$$-ról, de különösen az, ahogyan beszél róla, nagyon megfogott.

Azért keresem most, mert egy hasonló témájú platformon dolgozom. ($$$)

Röviden, úgy gondolom, hogy az együttműködés mindkét oldal számára előnyös lehet. Egy javaslatom van: egy vendégcikk megírása (írnék Önnek egy körülbelül 1500 szavas cikket, amelyet az Ön által választott kulcsszóra optimalizálok, nem mesterséges intelligenciával generált, és mélyreható SEO-munkát tartalmaz).

Mindenesetre köszönöm még egyszer a munkáját, és a válaszát (bármilyen is legyen az) nagyra értékelem.

Üdvözlettel,
Cyril Gendarme, a "$$$" csapat nevében""",
    "de": """Hallo!

Zunächst einmal möchte ich mich bei Ihnen für Ihre Arbeit bedanken, die ich vor ein paar Monaten entdeckt habe und die ich mehr als interessant fand. Ihre Sichtweise auf $$$, aber vor allem die Art und Weise, wie Sie darüber sprechen, hat mich beeindruckt.

Ich melde mich bei Ihnen, weil ich an einer Plattform arbeite, die ein ähnliches Thema behandelt. ($$$)

Kurz gesagt, ich denke, eine Zusammenarbeit könnte für beide Seiten von Vorteil sein. Ich habe ein Angebot für Sie: das Verfassen eines Gastartikels (ich schreibe für Sie einen Artikel mit etwa 1500 Wörtern, optimiert für ein von Ihnen gewähltes Schlüsselwort, nicht durch KI generiert und mit einer tiefgehenden SEO-Arbeit).

Vielen Dank nochmals für Ihre Arbeit, und Ihre Antwort (egal wie sie ausfällt) wird sehr geschätzt.

Mit freundlichen Grüßen,
Cyril Gendarme, im Namen des "$$$"-Teams""",
    "tr": """Merhaba!

Öncelikle, birkaç ay önce keşfettiğim ve son derece ilginç bulduğum çalışmalarınız için teşekkür etmek istiyorum. $$$ konusundaki vizyonunuz ve özellikle bunu ele alış biçiminiz beni etkiledi.

Bugün size ulaşmamın sebebi, oldukça benzer bir temaya sahip bir platform üzerinde çalışıyor olmamdır. ($$$)

Kısacası, bir iş birliğinin her iki sitemize de fayda sağlayabileceğini düşünüyorum. Sizin için bir önerim var: bir misafir yazısı yazmak (yaklaşık 1500 kelimelik, sizin seçtiğiniz bir anahtar kelimeye göre optimize edilmiş, yapay zeka ile oluşturulmamış ve derin bir SEO çalışması içeren bir makale yazacağım).

Her halükarda, çalışmalarınız için tekrar teşekkür ederim ve yanıtınızı (her ne olursa olsun) memnuniyetle bekliyorum.

Cyril Gendarme, "$$$" ekibi adına""",
    "no": """Hei!

Først og fremst vil jeg takke deg for arbeidet ditt som jeg oppdaget for noen måneder siden og fant svært interessant. Din visjon om $$$, og spesielt måten du snakker om det på, tiltalte meg veldig.

Jeg kontakter deg i dag fordi jeg jobber med en plattform med et ganske likt tema. ($$$)

Kort sagt, jeg tror et samarbeid kan være fordelaktig for begge våre nettsider. Jeg har et forslag til deg: skriving av en gjesteartikkel (jeg vil skrive en artikkel for deg på rundt 1500 ord, optimalisert for et nøkkelord du velger, ikke generert med AI og med et dypt SEO-arbeid).

Uansett, takk igjen for arbeidet ditt, og svaret ditt (uansett hva det måtte være) settes pris på.

Med vennlig hilsen,
Cyril Gendarme, for "$$$"-teamet""",
    "pt": '''Olá!

Antes de mais nada, gostaria de agradecer pelo seu trabalho, que descobri há alguns meses e achei mais do que interessante. Sua visão sobre $$$, e especialmente a forma como você fala sobre isso, me chamou a atenção.

Se estou entrando em contato com você hoje, é porque estou trabalhando em uma plataforma com um tema bastante similar. ($$$)

Em resumo, acredito que uma colaboração poderia beneficiar ambos os nossos sites. Tenho uma proposta para você: a redação de um artigo como convidado (escreverei um artigo para você com cerca de 1500 palavras, otimizado para uma palavra-chave de sua escolha, não gerado com IA e com um trabalho profundo de SEO).

De qualquer forma, obrigado novamente pelo seu trabalho, e sua resposta (seja ela qual for) será muito apreciada.

Cyril Gendarme, pela equipe de "$$$"''',
    "af": """Hallo!

Eerstens wil ek u bedank vir u werk wat ek 'n paar maande gelede ontdek het en uiters interessant gevind het. U siening van $$$, en veral die manier waarop u daaroor praat, het my aangespreek.

Ek kontak u vandag omdat ek aan 'n platform werk met 'n soortgelyke tema. ($$$)

Kortom, ek dink 'n samewerking kan beide ons webwerwe bevoordeel. Ek het 'n voorstel vir u: die skryf van 'n gasartikel (ek sal 'n artikel van ongeveer 1500 woorde vir u skryf, geoptimaliseer vir 'n sleutelwoord van u keuse, nie met KI geskep nie en met diepgaande SEO-werk).

In elk geval, dankie weereens vir u werk, en u antwoord (wat dit ook al mag wees) sal waardeer word.

Cyril Gendarme, namens die "$$$"-span""",
    "pl": '''Cześć!

Przede wszystkim chciałbym podziękować za Twoją pracę, którą odkryłem kilka miesięcy temu i która wydała mi się niezwykle interesująca. Twoja wizja $$$, a zwłaszcza sposób, w jaki o tym mówisz, bardzo mi się spodobała.

Kontaktuję się z Tobą, ponieważ pracuję nad platformą o dość podobnej tematyce. ($$$)

Krótko mówiąc, uważam, że współpraca mogłaby przynieść korzyści obu naszym stronom. Mam dla Ciebie propozycję: napisanie gościnnego artykułu (napiszę dla Ciebie artykuł o długości około 1500 słów, zoptymalizowany pod wybrane przez Ciebie słowo kluczowe, nie generowany przez AI i z głęboką pracą SEO).

W każdym razie jeszcze raz dziękuję za Twoją pracę, a Twoja odpowiedź (niezależnie od jej treści) będzie mile widziana.

Cyril Gendarme, z ramienia zespołu "$$$"''',
    "it": '''Ciao!

Prima di tutto, vorrei ringraziarti per il tuo lavoro, che ho scoperto qualche mese fa e ho trovato più che interessante. La tua visione di $$$, e soprattutto il modo in cui ne parli, mi ha colpito.

Se ti contatto oggi, è perché sto lavorando su una piattaforma con un tema piuttosto simile. ($$$)

In breve, penso che una collaborazione potrebbe portare benefici a entrambi i nostri siti. Ho una proposta per te: la scrittura di un articolo ospite (scriverò un articolo di circa 1500 parole, ottimizzato per una parola chiave di tua scelta, non generato con IA e con un profondo lavoro di SEO).

In ogni caso, grazie ancora per il tuo lavoro, e la tua risposta (qualunque essa sia) sarà apprezzata.

Cyril Gendarme, per il team di "$$$"''',
    "id": '''Halo!

Pertama-tama, saya ingin mengucapkan terima kasih atas pekerjaan Anda yang saya temukan beberapa bulan lalu dan sangat menarik bagi saya. Pandangan Anda tentang $$$, terutama cara Anda membicarakannya, sangat menarik perhatian saya.

Jika saya menghubungi Anda hari ini, itu karena saya sedang mengerjakan platform dengan tema yang cukup mirip. ($$$)

Singkatnya, saya pikir kolaborasi dapat menguntungkan kedua situs kami. Saya punya proposal untuk Anda: penulisan artikel tamu (saya akan menulis artikel untuk Anda sekitar 1500 kata, dioptimalkan untuk kata kunci pilihan Anda, tidak dihasilkan oleh AI, dan dengan pekerjaan SEO yang mendalam).

Bagaimanapun, terima kasih sekali lagi atas pekerjaan Anda, dan tanggapan Anda (apapun itu) sangat dihargai.

Cyril Gendarme, untuk tim "$$$"''',
    "ro": '''Bună!

În primul rând, aș dori să vă mulțumesc pentru munca dumneavoastră pe care am descoperit-o acum câteva luni și pe care am găsit-o extrem de interesantă. Viziunea dumneavoastră despre $$$, dar mai ales modul în care vorbiți despre acest subiect, m-a impresionat.

Dacă vă contactez astăzi, este pentru că lucrez la o platformă cu o tematică destul de similară. ($$$)

Pe scurt, cred că o colaborare ar putea aduce beneficii ambelor site-uri. Am o propunere pentru dumneavoastră: redactarea unui articol invitat (voi scrie un articol de aproximativ 1500 de cuvinte, optimizat pentru un cuvânt cheie la alegerea dumneavoastră, nu generat cu IA și cu o muncă SEO aprofundată).

Oricum, vă mulțumesc încă o dată pentru munca dumneavoastră, iar răspunsul dumneavoastră (oricare ar fi acesta) este apreciat.

Cyril Gendarme, din partea echipei "$$$"''',
    "nl": """Hallo!

Allereerst wil ik u bedanken voor uw werk dat ik een paar maanden geleden ontdekte en buitengewoon interessant vond. Uw visie op $$$, en vooral de manier waarop u erover spreekt, sprak mij enorm aan.

Ik neem vandaag contact met u op omdat ik werk aan een platform met een vergelijkbaar thema. ($$$)

Kortom, ik denk dat een samenwerking voordelig kan zijn voor beide websites. Ik heb een voorstel voor u: het schrijven van een gastartikel (ik schrijf een artikel van ongeveer 1500 woorden voor u, geoptimaliseerd voor een trefwoord naar keuze, niet gegenereerd door AI en met diepgaand SEO-werk).

Hoe dan ook, nogmaals bedankt voor uw werk, en uw antwoord (wat het ook moge zijn) wordt zeer gewaardeerd.

Cyril Gendarme, namens het "$$$"-team""",
    "ca": """Hola!

En primer lloc, m’agradaria agrair-te la teva feina, que vaig descobrir fa uns mesos i que vaig trobar més que interessant. La teva visió sobre $$$, però especialment la manera com en parles, em va captivar.

Si em poso en contacte amb tu avui, és perquè estic treballant en una plataforma amb una temàtica força similar. ($$$)

En resum, crec que una col·laboració podria beneficiar ambdues pàgines web. Tinc una proposta per a tu: l’escriptura d’un article com a convidat (escriuré un article per a tu d’unes 1500 paraules, optimitzat per a una paraula clau de la teva elecció, no generat amb IA i amb un treball profund de SEO).

En qualsevol cas, moltes gràcies de nou per la teva feina, i la teva resposta (sigui quina sigui) serà molt apreciada.

Cyril Gendarme, per l’equip de "$$$""",
    "sk": """Ahoj!

Najprv by som vám chcel poďakovať za vašu prácu, ktorú som objavil pred niekoľkými mesiacmi a ktorá ma viac ako zaujala. Váš pohľad na $$$, a najmä spôsob, akým o tom hovoríte, ma oslovil.

Dnes vás kontaktujem, pretože pracujem na platforme s pomerne podobnou témou. ($$$)

Stručne povedané, myslím si, že spolupráca by mohla byť prospešná pre obe naše stránky. Mám pre vás návrh: napísanie hosťovského článku (napíšem pre vás článok o rozsahu približne 1500 slov, optimalizovaný pre kľúčové slovo podľa vášho výberu, nevytvorený pomocou AI a s hĺbkovou SEO prácou).

V každom prípade vám ešte raz ďakujem za vašu prácu a vaša odpoveď (nech už bude akákoľvek) bude veľmi ocenená.

S úctou,
Cyril Gendarme, za tím "$$$""",
    "zh": '''你好！

首先，我想感谢您几个月前的作品，我发现它非常有趣。$$$的视角，尤其是您谈论它的方式，深深吸引了我。

今天联系您是因为我正在开发一个主题相当类似的平台。 ($$$)

简而言之，我认为合作可以使我们双方的网站受益。我有一个提议：撰写一篇客座文章（我将为您撰写一篇约1500字的文章，针对您选择的关键词进行优化，不通过AI生成，并经过深度SEO优化）。

无论如何，再次感谢您的工作，您的回复（无论内容如何）我都会非常感谢。

Cyril Gendarme，代表“$$$”团队"''',
    "da": """Hej!

Først og fremmest vil jeg gerne takke dig for dit arbejde, som jeg opdagede for nogle måneder siden og fandt yderst interessant. Din vision om $$$, og især måden du taler om det på, tiltalte mig meget.

Jeg kontakter dig i dag, fordi jeg arbejder på en platform med et ret lignende tema. ($$$)

Kort sagt, jeg tror, at et samarbejde kunne gavne begge vores hjemmesider. Jeg har et forslag til dig: skrivning af en gæsteartikel (jeg vil skrive en artikel til dig på cirka 1500 ord, optimeret til et nøgleord efter dit valg, ikke genereret med AI og med et dybt SEO-arbejde).

Under alle omstændigheder, tak igen for dit arbejde, og dit svar (uanset hvad det måtte være) bliver værdsat.

Venlig hilsen,
Cyril Gendarme, fra "$$$"-teamet""",
    "fi": """Hei!

Ensinnäkin haluan kiittää sinua työstäsi, jonka löysin muutama kuukausi sitten ja jota pidin erittäin mielenkiintoisena. Näkemyksesi $$$-aiheesta ja erityisesti tapa, jolla puhut siitä, teki minuun vaikutuksen.

Otan sinuun yhteyttä, koska työskentelen alustalla, jonka teema on melko samanlainen. ($$$)

Lyhyesti sanottuna uskon, että yhteistyö voisi hyödyttää molempia sivustojamme. Minulla on sinulle ehdotus: vieraskirjoituksen laatiminen (kirjoitan sinulle noin 1500 sanan artikkelin, joka on optimoitu valitsemallesi avainsanalle, ei AI:n tuottama ja syvällisesti hakukoneoptimoitu).

Joka tapauksessa kiitos vielä kerran työstäsi, ja vastauksesi (oli se mikä tahansa) on arvostettu.

Cyril Gendarme, "$$$"-tiimin puolesta""",
    "ja": """こんにちは！

まず最初に、数か月前に知ったあなたの作品に感謝したいと思います。それは非常に興味深いものでした。$$$に対するあなたのビジョン、特にその語り方がとても魅力的でした。

今日ご連絡するのは、似たテーマを持つプラットフォームに取り組んでいるからです。 ($$$)

要するに、コラボレーションはお互いのサイトにとって有益になると考えています。ご提案があります：ゲスト記事の執筆です（約1500文字の記事を執筆し、お選びいただいたキーワードに最適化し、AI生成ではなく、深いSEO作業を伴います）。

いずれにせよ、改めてあなたの作品に感謝し、どのような回答でも心から感謝します。

Cyril Gendarme
"$$$" チームを代表して""",
    "hr": '''Pozdrav!

Prije svega, želio bih vam zahvaliti na vašem radu koji sam otkrio prije nekoliko mjeseci i koji sam smatrao izuzetno zanimljivim. Vaša vizija $$$, a posebno način na koji govorite o tome, zaista me privukla.

Danas vas kontaktiram jer radim na platformi s prilično sličnom temom. ($$$)

Ukratko, mislim da bi suradnja mogla koristiti objema našim stranicama. Imam prijedlog za vas: pisanje gostujućeg članka (napisat ću članak od oko 1500 riječi za vas, optimiziran za ključnu riječ po vašem izboru, ne generiran pomoću AI-a i s dubinskim SEO radom).

U svakom slučaju, još jednom hvala na vašem radu, a vaš odgovor (bez obzira kakav bio) cijenit će se.

S poštovanjem,
Cyril Gendarme, za tim "$$$"''',
    "sv": """Hej!

Först och främst vill jag tacka dig för ditt arbete som jag upptäckte för några månader sedan och som jag fann mycket intressant. Din syn på $$$, och särskilt sättet du pratar om det på, tilltalade mig verkligen.

Jag kontaktar dig idag eftersom jag arbetar med en plattform med ett ganska liknande tema. ($$$)

Kort sagt, jag tror att ett samarbete skulle kunna gynna båda våra webbplatser. Jag har ett förslag till dig: skrivandet av en gästartikel (jag skriver en artikel åt dig på cirka 1500 ord, optimerad för ett nyckelord du väljer, inte genererad med AI och med djupgående SEO-arbete).

Hur som helst, tack igen för ditt arbete, och ditt svar (vad det än är) uppskattas mycket.

Med vänliga hälsningar,
Cyril Gendarme, för "$$$"-teamet""",
    "ct": '''Hola!

En primer lloc, m’agradaria agrair-te la teva feina, que vaig descobrir fa uns mesos i que vaig trobar més que interessant. La teva visió sobre $$$, però especialment la manera com en parles, em va captivar.

Si em poso en contacte amb tu avui, és perquè estic treballant en una plataforma amb una temàtica força similar. ($$$)

En resum, crec que una col·laboració podria beneficiar ambdues pàgines web. Tinc una proposta per a tu: l’escriptura d’un article com a convidat (escriuré un article per a tu d’unes 1500 paraules, optimitzat per a una paraula clau de la teva elecció, no generat amb IA i amb un treball profund de SEO).

En qualsevol cas, moltes gràcies de nou per la teva feina, i la teva resposta (sigui quina sigui) serà molt apreciada.

Cyril Gendarme, per l’equip de "$$$"''',
    "id": '''Halo!

Pertama-tama, saya ingin mengucapkan terima kasih atas karya Anda yang saya temukan beberapa bulan lalu dan saya anggap sangat menarik. Pandangan Anda tentang $$$, terutama cara Anda membahasnya, sangat menginspirasi saya.

Saya menghubungi Anda hari ini karena saya sedang mengembangkan platform dengan tema yang cukup mirip. ($$$)

Singkatnya, saya pikir kolaborasi bisa menguntungkan kedua situs kami. Saya memiliki proposal untuk Anda: penulisan artikel tamu (saya akan menulis artikel sekitar 1500 kata untuk Anda, dioptimalkan untuk kata kunci pilihan Anda, tidak dihasilkan oleh AI, dan disertai dengan kerja SEO yang mendalam).

Bagaimanapun, terima kasih sekali lagi atas karya Anda, dan tanggapan Anda (apapun itu) sangat dihargai.

Salam,
Cyril Gendarme, untuk tim "$$$"''',
    "ua": '''Вітаю!

Перш за все, хочу подякувати вам за вашу роботу, яку я відкрив кілька місяців тому і яка виявилася надзвичайно цікавою. Ваша бачення $$$, а особливо те, як ви про це говорите, мене глибоко вразило.

Сьогодні я звертаюсь до вас, тому що працюю над платформою з досить схожою тематикою. ($$$)

Коротко кажучи, я вважаю, що співпраця могла б принести користь обом нашим сайтам. У мене є пропозиція: написання гостьової статті (я напишу для вас статтю на приблизно 1500 слів, оптимізовану під ключове слово на ваш вибір, не створену за допомогою ШІ і з глибокою SEO-оптимізацією).

Ще раз дякую за вашу роботу, і ваша відповідь (незалежно від її змісту) буде дуже цінною.

З повагою,
Cyril Gendarme, від команди "$$$"''',
    "za": """Hallo!

Eerstens wil ek u bedank vir u werk wat ek 'n paar maande gelede ontdek het en uiters interessant gevind het. U siening van $$$, en veral die manier waarop u daaroor praat, het my aangespreek.

Ek kontak u vandag omdat ek aan 'n platform werk met 'n soortgelyke tema. ($$$)

Kortom, ek dink 'n samewerking kan beide ons webwerwe bevoordeel. Ek het 'n voorstel vir u: die skryf van 'n gasartikel (ek sal 'n artikel van ongeveer 1500 woorde vir u skryf, geoptimaliseer vir 'n sleutelwoord van u keuse, nie met KI geskep nie en met diepgaande SEO-werk).

In elk geval, dankie weereens vir u werk, en u antwoord (wat dit ook al mag wees) sal waardeer word.

Cyril Gendarme, namens die "$$$"-span""",
}


def replace_placeholders(
    template: str, site_name: str, site_url: str, topic: str
) -> str:
    return replace_multiple_regex_in_string(template, [topic, site_url, site_name])


site_name_mapping = {}


def map_site_name(input: str) -> str:
    return site_name_mapping.get(input, input)


site_url_mapping = {"Belgae": "https://www.belgae.be"}


def map_site_to_url(input: str) -> str:
    result = site_url_mapping.get(input, "null")
    if result == "null":
        raise Exception(f"Site {input} not mapped to any url")
    return result
