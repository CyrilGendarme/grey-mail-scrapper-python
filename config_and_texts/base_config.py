# Debug=True will log what is done in the console + avoid lines to be removed in queries.txt
DEBUG = True
BASE_APP_DIR = "C:\\Users\\User\\Desktop\\netlinking\\"
CONFIG_FILE = "_config.txt"
PROJECTS_LIST = [
    "Belgae",
    # "Cgen",
    # "Gallery Minimalist",
    # "LPDB",
    # "Sensei Editions",
]
DB_NAME = "netlinking"
QUERIES_TABLE_NAME = "queries"
MAILS_TABLE_NAME = "mails"
MAILS_TIMESTAMP_TABLE_NAME = "mails_timestamp"
