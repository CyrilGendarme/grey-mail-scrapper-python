# NEXT STEPS

# Flow

-   determine config (lang, website)
-   create mail template (liquid)
-   search -> parse searchs -> list of mails
-   for each mail -> check if already in DB -> if not, add and send a mail
-   run through cron

# TBD by Miguel :

-   extension of placeholders_content_x to all langs
-   grammar verification for content with replaced placeholders for all languages

# Nice-to-have

listener on mail : if response receive from a mail send not long ago, put it in trash (automatic response)

# Extension as a web app

https://chatgpt.com/c/67a9ad89-ad34-8006-b387-8f1fae46f3b0
