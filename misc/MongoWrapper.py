from datetime import datetime, timezone, timedelta
from bson import ObjectId
from pymongo.errors import DuplicateKeyError
import os
import sys
import time
from typing import Dict, Union

root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.append(root_dir)

from config_and_texts.base_config import (
    BASE_APP_DIR,
    CONFIG_FILE,
    MAILS_TIMESTAMP_TABLE_NAME,
    QUERIES_TABLE_NAME,
    MAILS_TABLE_NAME,
)
from submodules.config_manager.project_dynamic_config import ProjectConfig
from submodules.mongodb_dao.MongoDAO import MongoDAO


class MongoWrapper:
    def __init__(
        self,
        config_manager: ProjectConfig,
    ):
        """
        Initialize the MongoWrapper with the necessary collections and DAO object.
        """
        self.dao = MongoDAO(
            db_name=config_manager.get("DB_NAME"),
            uri=config_manager.get("DB_URI"),
            username=config_manager.get("DB_USER"),
            password=config_manager.get("DB_PWD"),
            tls_ca_file=os.path.join(root_dir, "certs", "ca.pem"),
            tls_cert_file=os.path.join(
                root_dir, "certs", "server_combined_backlinapp-cyrilhome.pem"
            ),
            tls_allow_invalid_certificates=True,
        )
        self._ensure_collections_exist()

    def _ensure_collections_exist(self) -> None:
        """
        Ensure both collections exist, creating indexes as needed.
        """
        # Ensure the first table exists and create necessary indexes
        if not self.dao.exists(QUERIES_TABLE_NAME, {}):
            print(f"{QUERIES_TABLE_NAME} doesn't exist. Creating collection.")
            self.dao.create_index(QUERIES_TABLE_NAME, "project")
            self.dao.create_index(QUERIES_TABLE_NAME, "lang")
            self.dao.create_index(QUERIES_TABLE_NAME, "query")
            self.dao.create_index(QUERIES_TABLE_NAME, "tried_number")

        # Ensure the second table exists and create necessary indexes
        if not self.dao.exists(MAILS_TABLE_NAME, {}):
            print(f"{MAILS_TABLE_NAME} doesn't exist. Creating collection.")
            self.dao.create_index(MAILS_TABLE_NAME, "website", unique=True)
            self.dao.create_index(MAILS_TABLE_NAME, "queries_table_unique_id")

    def add_queries_table_entry(
        self, project: str, lang: str, query: str
    ) -> Dict[str, Union[bool, str]]:
        """
        Add an entry in the first table if it doesn't already exist.
        Ensure the tried_number field is set to 0 on creation.
        """
        query_data = {"project": project, "lang": lang, "query": query}

        # Check if the entry already exists
        if not self.dao.exists(QUERIES_TABLE_NAME, query_data):
            # Add tried_number set to 0 if the entry does not exist
            entry_data = {**query_data, "tried_number": 0}
            result = self.dao.insert_one(QUERIES_TABLE_NAME, entry_data)
            return result
        else:
            return {"success": False, "error": "Entry already exists in first table"}

    def get_queries_table_entry(
        self, project: str, lang: str, query: str
    ) -> Dict[str, Union[bool, str]]:
        """
        Get an entry from the first table based on project, lang, and query.
        """
        query_data = {"project": project, "lang": lang, "query": query}
        return self.dao.find_one(QUERIES_TABLE_NAME, query_data)

    def increment_queries_table_tried_number(
        self, project: str, lang: str, query: str
    ) -> Dict[str, int]:
        """
        Increment the `tried_number` field of an entry in the first table.
        If it doesn't exist, create it with tried_number set to 1.
        """
        query_data = {"project": project, "lang": lang, "query": query}

        # Try to find the existing entry
        existing_entry = self.dao.find_one(QUERIES_TABLE_NAME, query_data)

        if not existing_entry:
            return {
                "success": False,
                "message": "No entry found in 2nd table",
            }

        # If the entry exists, increment the `tried_number`
        self.dao.increment_one(
            QUERIES_TABLE_NAME, query_data, "tried_number", increment_value=1
        )
        return {"success": True, "message": "Tried number incremented."}

    def get_queries_table_tried_number(
        self, project: str, lang: str, query: str
    ) -> Dict[str, Union[bool, int, str]]:
        """
        Retrieve the `tried_number` field from the first table based on project, lang, and query.
        """
        query_data = {"project": project, "lang": lang, "query": query}
        entry = self.dao.find_one(QUERIES_TABLE_NAME, query_data)

        if entry:
            entry.get("tried_number", 0)
        else:
            raise ValueError("Entry not found in first table")

    def add_mails_table_entry(
        self,
        website: str,
        mails: list,
        status: bool,
        project: str,
        lang: str,
        query: str,
    ) -> Dict[str, Union[bool, str]]:
        """
        Add an entry in the second table and associate it with an entry in the first table.
        If the entry in the first table does not exist, it will be created.
        """
        # Check if the entry exists in the first table
        queries_table_entry = self.get_queries_table_entry(project, lang, query)

        # If it doesn't exist, create it
        if not queries_table_entry:
            queries_table_entry = self.add_queries_table_entry(project, lang, query)
            if not queries_table_entry.get("success"):
                return {"success": False, "error": "Failed to create first table entry"}

        # Prepare the document to be inserted in the second table
        document = {
            "website": website,
            "mails": mails,
            "status": status,
            "queries_table_unique_id": queries_table_entry["_id"],
        }

        timestamp = datetime.now(timezone.utc)
        for mail in mails:
            self.add_mails_timestamp_table_entry(mail, timestamp)

        # Insert the document into the second table
        try:
            return self.dao.insert_one(MAILS_TABLE_NAME, document)
        except DuplicateKeyError:
            return {
                "success": False,
                "error": "Duplicate website entry in second table",
            }

    def add_mails_timestamp_table_entry(self, mail: str, timestamp: int):
        # Prepare the document to be inserted in the second table
        document = {"mail": mail, "timestamp": timestamp}

        # Insert the document into the second table
        try:
            return self.dao.insert_one(MAILS_TIMESTAMP_TABLE_NAME, document)
        except DuplicateKeyError:
            return {
                "success": False,
                "error": "Duplicate website entry in third table",
            }

    def is_mail_in_mails_timestamp_table(self, mail: str):
        query_data = {"mail": mail}
        return self.dao.exists(MAILS_TIMESTAMP_TABLE_NAME, query_data)

    def get_all_mails_in_mails_timestamp_table(self):
        """
        Retrieves all the mails from the 'MAILS_TIMESTAMP_TABLE_NAME'.
        Returns a list of mails.
        """
        try:
            # Query all documents from the table
            result = self.dao.find_all(MAILS_TIMESTAMP_TABLE_NAME)

            # Extract the 'mail' field from each document and return as a list
            mails = [entry["mail"] for entry in result]
            return mails
        except Exception as e:
            return {
                "success": False,
                "error": str(e),
            }

    def clean_mails_from_mails_timestamp_table(self):
        cutoff_timestamp = datetime.now(timezone.utc) - timedelta(minutes=1)
        query = {"timestamp": {"$lte": cutoff_timestamp}}
        self.dao.delete_many(MAILS_TIMESTAMP_TABLE_NAME, query)

    def get_mails_table_status(
        self, website: str, lang: str, project: str, query: str
    ) -> bool:
        """
        Check if a website exists in the second table for a given lang (via first table reference)
        and return its status.
        """
        # Step 1: Find the entry in the first table with the matching website and lang
        queries_table_entry = self.get_queries_table_entry(project, lang, query)
        if not queries_table_entry:
            return False

        # Step 2: Use the queries_table_unique_id from the first table to query the second table
        queries_table_unique_id = ObjectId(queries_table_entry["_id"])

        mails_table_entry = self.dao.find_one(
            MAILS_TABLE_NAME,
            {
                "queries_table_unique_id": queries_table_unique_id,
                "website": website,
            },
        )

        if mails_table_entry:
            return mails_table_entry["status"]

        return False

    def modify_mails_table_status(
        self, website: str, lang: str, project: str, query: str, new_status: bool
    ) -> Dict[str, int]:
        """
        Modify the status of an entry in the second table.
        """
        # Step 1: Find the entry in the first table with the matching website and lang
        queries_table_entry = self.get_queries_table_entry(project, lang, query)
        if not queries_table_entry:
            return {"success": False, "error": "No matching entry in first table"}

        # Step 2: Use the queries_table_unique_id from the first table to query the second table
        queries_table_unique_id = queries_table_entry["_id"]
        query_data = {
            "queries_table_unique_id": queries_table_unique_id,
            "website": website,
        }
        update_data = {"status": new_status}

        return self.dao.update_one(MAILS_TABLE_NAME, query_data, update_data)


if __name__ == "__main__":
    config = ProjectConfig(
        os.path.join(BASE_APP_DIR, "Belgae", CONFIG_FILE),
        [
            "SMTP_SERVER",
            "SMTP_PORT",
            "EMAIL_ADDRESS",
            "EMAIL_PASSWORD",
            "DB_USER",
            "DB_PWD",
            "DB_NAME",
            "DB_URI",
        ],
        {
            "SMTP_SERVER": "",
            "SMTP_PORT": 465,
            "EMAIL_ADDRESS": "",
            "EMAIL_PASSWORD": "",
            "DB_USER": "",
            "DB_PWD": "",
            "DB_NAME": "",
            "DB_URI": "",
        },
    )
    mongo_wrapper = MongoWrapper(config)

    print("Creating first table entry for project1, lang 'en', and query 'query1'")
    print(mongo_wrapper.add_queries_table_entry("project1", "en", "query1"))

    print("\nCreating second table entry for website 'example.com'")
    print(
        mongo_wrapper.add_mails_table_entry(
            website="example.com",
            mails=["contact@example.com"],
            status=True,
            project="project1",
            lang="en",
            query="query1",
        )
    )

    print(
        "\nGet second table status for website 'example.com' (requires lang, project, and query)"
    )
    print(
        mongo_wrapper.get_mails_table_status("example.com", "en", "project1", "query1")
    )

    print("\nModify status of second table entry for website 'example.com'")
    print(
        mongo_wrapper.modify_mails_table_status(
            "example.com", "en", "project1", "query1", False
        )
    )

    print(
        "\nGet updated second table status for website 'example.com' (requires lang, project, and query)"
    )
    print(
        mongo_wrapper.get_mails_table_status("example.com", "en", "project1", "query1")
    )

    print(
        "\nIncrement tried_number for first table entry 'project1', lang 'en', query 'query1'"
    )
    print(
        mongo_wrapper.increment_queries_table_tried_number("project1", "en", "query1")
    )
    print(
        "\nIncrement tried_number for first table entry 'project1', lang 'en', query 'query1'"
    )
    print(
        mongo_wrapper.increment_queries_table_tried_number("project1", "en", "query1")
    )

    # Step 3: Clean old entries from the mails_timestamp table
    print("\nCleaning old entries from the mails_timestamp table (older than 1 minute)")
    mongo_wrapper.clean_mails_from_mails_timestamp_table()

    # Optional: Add a sleep to ensure that timestamps have passed a minute to test the deletion
    print("\nWaiting 1 minute to ensure some entries can be deleted...")
    time.sleep(60)  # Wait for 60 seconds

    # Now you can clean again, or you can directly test entries in mails_timestamp after cleaning.
    print("\nCleaning again after waiting 1 minute to see the deletion action.")
    mongo_wrapper.clean_mails_from_mails_timestamp_table()

    # Verify the status after deletion
    print("\nVerifying if any old entries exist in mails_timestamp table")
    # This can be extended with actual verification by finding entries older than 1 minute if desired.
