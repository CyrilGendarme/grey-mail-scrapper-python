import codecs
from datetime import datetime
import os
import sys

root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.append(root_dir)

from config_and_texts.base_config import BASE_APP_DIR, DEBUG

encoding = "utf-8-sig"

mail_file = ""
website_file = ""
failed_website_file = ""
query = ""
lang = ""
website = ""


def write_debug_line(text):

    # # Keep it stub, unless some debugging is required
    # pass

    current_datetime = datetime.now()

    debug_file = BASE_APP_DIR + "debug.txt"
    if not os.path.isfile(debug_file):
        # If the file doesn't exist, create it
        with open(debug_file, mode="w", encoding="utf-8") as file:
            pass
    with open(debug_file, mode="a", encoding="utf-8") as file:
        file.write(current_datetime.strftime("%Y-%m-%d %H:%M:%S") + "\n" + text + "\n")
        if DEBUG:
            print(text)


def write_lines_to_file(file_path, lines, encoding="utf-8-sig"):
    with codecs.open(file_path, "w", encoding=encoding, errors="replace") as file:
        file.writelines(lines)


def populate_queries_from_text_file(file_path):
    data_list = []  # To store the resulting JSON objects
    website_data = None

    with open(file_path, "r", encoding=encoding, errors="replace") as file:
        lines = file.readlines()
        i = 0

        while i < len(lines):
            line = lines[i].strip()

            if line.strip():  # no empty lines
                lang, subject, query = line.split(" - ", 2)

                lang = lang.strip()
                subject = subject.strip()
                query = query.strip()
                website_data = {"lang": lang, "subject": subject, "query": query}
                data_list.append(website_data)

            i += 1

    # Write the modified lines back to the file
    write_lines_to_file(file_path, lines, encoding="utf-8-sig")

    return data_list


def pop_first_query_from_text_file(file_path):

    if DEBUG:
        return

    with open(file_path, "r", encoding=encoding, errors="replace") as file:
        lines = file.readlines()
        i = 0

        while i < len(lines):
            if lines[i].strip().strip():  # no empty lines
                lines.pop(i)
            i += 1

    # Write the modified lines back to the file
    write_lines_to_file(file_path, lines, encoding="utf-8-sig")
