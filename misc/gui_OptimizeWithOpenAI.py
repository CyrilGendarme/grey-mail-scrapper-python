import os
import sys

root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.append(root_dir)

from submodules.python_gui.TextGenerationFromText import TextGenerationFromText
from submodules.python_gui.app_instance import q_application


class OptimizeWithOpenAI(TextGenerationFromText):
    def __init__(self, initial_text1="", initial_text2="", processing_function=None):
        super().__init__(
            title="Optimize with openai",
            button1_text="Regenerate Text",
            button2_text="Validate",
        )
        self.text_input1.setText(initial_text1)
        self.text_input2.setText(initial_text2)
        self.processing_function = processing_function or (lambda text: text.upper())
        self.button1_signal.connect(self.on_regenerate_right_text)
        self.button2_signal.connect(self.on_return_and_close)
        self.returned_text = None

    def on_regenerate_right_text(self):
        processed_text = self.processing_function(
            f"Correct and optimize that mail body : \n\n\n\n{self.text_input1.toPlainText()}",
            False,
        )
        self.text_input2.setText(processed_text)

    def on_return_and_close(self, text):
        self.returned_text = text  # Store the returned text
        self.close()

    @staticmethod
    def run(text1: str, text2: str, processing_function) -> str:
        window = OptimizeWithOpenAI(text1, text2, processing_function)
        window.show()
        q_application.exec()
        return window.returned_text
