import os

from config_and_texts.base_config import PROJECTS_LIST, BASE_APP_DIR, CONFIG_FILE
from misc.MongoWrapper import MongoWrapper
from submodules.config_manager.project_dynamic_config import ProjectConfig
from tasks.GoogleScraper import GoogleScraper
from tasks.MailProcessor import MailProcessor

if __name__ == "__main__":
    for project in PROJECTS_LIST:
        config = ProjectConfig(
            os.path.join(BASE_APP_DIR, project, CONFIG_FILE),
            [
                "SMTP_SERVER",
                "SMTP_PORT",
                "EMAIL_ADDRESS",
                "EMAIL_PASSWORD",
                "DB_USER",
                "DB_PWD",
                "DB_NAME",
                "DB_URI",
            ],
            {
                "SMTP_SERVER": "",
                "SMTP_PORT": 465,
                "EMAIL_ADDRESS": "",
                "EMAIL_PASSWORD": "",
                "DB_USER": "",
                "DB_PWD": "",
                "DB_NAME": "",
                "DB_URI": "",
            },
        )
        mongo_wrapper = MongoWrapper(config)
        mail_processor = MailProcessor(project, config)
        google_scrapper = GoogleScraper(project, config, mail_processor, mongo_wrapper)
        google_scrapper.run()
