from datetime import datetime, timedelta
import email
from email.header import decode_header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import imaplib
import os
import pytz
import smtplib
import sys
from typing import List

root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.append(root_dir)

from config_and_texts.base_config import BASE_APP_DIR, CONFIG_FILE, DEBUG
import config_and_texts.mail_content as mail_content
from misc import gui_OptimizeWithOpenAI
from submodules.aigen_common.openAI_text import makeAnApiCall
from submodules.config_manager.project_dynamic_config import ProjectConfig


class MailProcessor:
    def __init__(self, project: str, config_manager: ProjectConfig):
        self.project = project
        self.SMTP_SERVER = config_manager.get("SMTP_SERVER")
        self.SMTP_PORT = config_manager.get("SMTP_PORT")
        self.EMAIL_ADDRESS = config_manager.get("EMAIL_ADDRESS")
        self.EMAIL_PASSWORD = config_manager.get("EMAIL_PASSWORD")
        self.IMAP_SERVER = self.SMTP_SERVER  # FIXME? should IMAP_SERVER be specific?
        self.lang = ""
        self.body = ""

    def set_lang_and_subject(self, lang, subject):
        if not (lang == self.lang and subject == self.subject):
            self.subject = subject
            self.title = mail_content.MAIL_TITLE[lang]

            base_body = mail_content.replace_placeholders(
                mail_content.MAIL_BODY[lang],
                mail_content.map_site_name(self.project),
                mail_content.map_site_to_url(self.project),
                self.subject,
            )

            if DEBUG:
                self.body = gui_OptimizeWithOpenAI.OptimizeWithOpenAI.run(
                    text1=base_body,
                    text2=base_body,
                    processing_function=makeAnApiCall,
                )
            else:
                self.body = gui_OptimizeWithOpenAI.OptimizeWithOpenAI.run(
                    text1=base_body,
                    text2=makeAnApiCall(
                        f"Correct and optimize that mail body : \n\n\n\n{base_body}",
                        False,
                    ),
                    processing_function=makeAnApiCall,
                )

            self.lang = lang

    def send_emails(self, to_emails: List[str]) -> None:
        message = MIMEMultipart()
        message["From"] = self.EMAIL_ADDRESS
        message["Subject"] = self.title
        message["Bcc"] = ", ".join(to_emails)
        message.attach(MIMEText(self.body, "plain"))

        with smtplib.SMTP_SSL(self.SMTP_SERVER, self.SMTP_PORT) as server:
            server.login(self.EMAIL_ADDRESS, self.EMAIL_PASSWORD)
            server.send_message(message)

    def send_email(self, to_email: str):
        message = MIMEMultipart()
        message["From"] = self.EMAIL_ADDRESS
        message["To"] = to_email
        message["Subject"] = self.title
        message.attach(MIMEText(self.body, "plain"))

        with smtplib.SMTP_SSL(self.SMTP_SERVER, self.SMTP_PORT) as server:
            server.login(self.EMAIL_ADDRESS, self.EMAIL_PASSWORD)
            server.send_message(message)

    def check_and_delete_mails(self, mails_db, condition_function):

        mail = imaplib.IMAP4_SSL(self.IMAP_SERVER)
        mail.login(self.EMAIL_ADDRESS, self.EMAIL_PASSWORD)

        mail.select("inbox")
        belgium_tz = pytz.timezone("Europe/Brussels")
        current_time_belgium = datetime.now(belgium_tz)
        current_time_utc = current_time_belgium.astimezone(pytz.utc)
        one_minute_ago = current_time_utc - timedelta(minutes=10)
        # Only retrieve emails received from today
        time_since = current_time_utc.strftime("%d-%b-%Y")
        status, messages = mail.search(None, f'SINCE "{time_since}"')
        # Convert messages to a list of email IDs
        email_ids = messages[0].split()

        if not email_ids:
            print("No recent emails found.")
            return

        for email_id in email_ids:
            try:
                status, msg_data = mail.fetch(email_id, "(RFC822)")

                if status != "OK":
                    print(f"Failed to fetch email with ID: {email_id}")
                    continue

                for response_part in msg_data:
                    if isinstance(response_part, tuple):
                        msg = email.message_from_bytes(response_part[1])
                        from_ = msg.get("From")

                        # Parse the date and convert it to UTC
                        date_tuple = email.utils.parsedate_tz(msg.get("Date"))
                        if date_tuple:
                            # Convert to UTC timestamp + make the email's date timezone-aware (in UTC)
                            timestamp = email.utils.mktime_tz(date_tuple)
                            email_date = datetime.utcfromtimestamp(timestamp)
                            email_date = pytz.utc.localize(email_date)

                            # # Debugging: Show the comparison of email date and the 1-minute window in UTC
                            # print(f"Email received at: {email_date} (UTC)")
                            # print(f"Comparing with: {one_minute_ago} (UTC)")

                            if email_date >= one_minute_ago and condition_function(
                                from_, mails_db, email_date
                            ):
                                mail.store(email_id, "+FLAGS", "\\Deleted")
                                mail.expunge()
                                print(f"Deleted email: {msg}")
                            else:
                                print(
                                    f"Email received before threshold or condition not met, skipping: {msg}"
                                )

            except Exception as e:
                print(f"Error fetching email with ID {email_id}: {e}")
                continue

        mail.logout()


def is_automatic_response(from_: str, mails: List[str], timestamp: datetime):
    # Example condition: delete emails older than 7 days
    cutoff_time = datetime.now(pytz.utc) - timedelta(days=7)  # emails older than 7 days
    if timestamp < cutoff_time:
        return True  # Return True to delete the email
    return False


if __name__ == "__main__":
    config = ProjectConfig(
        os.path.join(BASE_APP_DIR, "Belgae", CONFIG_FILE),
        [
            "SMTP_SERVER",
            "SMTP_PORT",
            "EMAIL_ADDRESS",
            "EMAIL_PASSWORD",
            "DB_USER",
            "DB_PWD",
            "DB_NAME",
            "DB_URI",
        ],
        {
            "SMTP_SERVER": "",
            "SMTP_PORT": 465,
            "EMAIL_ADDRESS": "",
            "EMAIL_PASSWORD": "",
            "DB_USER": "",
            "DB_PWD": "",
            "DB_NAME": "",
            "DB_URI": "",
        },
    )

    # Create an instance of the MailProcessor class
    mail_processor = MailProcessor("foo", config)

    mail_processor.set_lang_and_subject("fr", "la mode")
    # mail_processor.send_email("cyrilgendarme@gmail.com")
    # mail_processor.send_emails(
    #     ["cyrilgendarme@gmail.com", "cyrilgendarme123@gmail.com"]
    # )
