from bs4 import BeautifulSoup
import json
from googlesearch import search
import os
import random
import re
import requests
import sys
import time

root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.append(root_dir)

from config_and_texts.base_config import BASE_APP_DIR, DEBUG
from config_and_texts.scrap_config import (
    QUERIES_FILE_SLUG,
    get_ltd_from_lang,
    SUBSTRINGS_TO_EXCLUDE_IN_MAILS,
    NB_DOMAINS_TO_CHECK,
    WEBSITES_TO_AVOID,
    MINIMAL_SATISFYING_NB_OF_RESULTS,
)
from config_and_texts.user_agent import get_random_user_agent
from config_and_texts.slugs import SLUG_DICT
import misc.files_mgmt as files_mgmt
from misc.MongoWrapper import MongoWrapper
from submodules.config_manager.project_dynamic_config import ProjectConfig
from tasks.MailProcessor import MailProcessor


class GoogleScraper:
    def __init__(
        self,
        project: str,
        config_manager: ProjectConfig,
        mail_processor: MailProcessor,
        mongo_wrapper: MongoWrapper,
    ):
        self.project = project
        self.mongo_wrapper = mongo_wrapper
        self.mail_processor = mail_processor

    @staticmethod
    def get_unique_emails_from_html(html_content):
        email_pattern = r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b"
        emails = re.findall(email_pattern, html_content)
        filtered_emails = [
            email
            for email in emails
            if not any(
                keyword.lower() in email.lower()
                for keyword in SUBSTRINGS_TO_EXCLUDE_IN_MAILS
            )
        ]
        return list(set(filtered_emails))

    @staticmethod
    def filter_on_best_emails(emails):
        best_email_prefixes = [
            "contact@",
            "info@",
            "support@",
            "contactus@",
            "contact_us@",
        ]

        # Find the first email that matches any prefix
        best_email = next(
            (
                email
                for email in emails
                for prefix in best_email_prefixes
                if email.startswith(prefix)
            ),
            None,
        )

        # Return the best email as a single-item list if found, otherwise return the full emails list
        return [best_email] if best_email else emails

    @staticmethod
    def bypass_email_protection(response):
        patterns_to_replace = [
            "[at]",
            "\{at\}",
            "(at)",
            "[@]",
            "\{@\}",
            "(@)",
            "[dot]",
            "\{dot\}",
            "(dot)",
            "[.]",
            "\{.\}",
            "(.)",
        ]
        for pattern in patterns_to_replace:
            response = response.replace(pattern, "@")
        return response

    @staticmethod
    def remove_invalid_chars(response):
        return response.replace("u003e", "")

    @staticmethod
    def normalize_url(title):
        title = title.replace("https://", "").replace("http://", "").split("/")[0]
        return "https://" + title

    @staticmethod
    def get_website_language(url):
        try:
            response = requests.get(url)
            if response is not None:
                language = response.headers.get("Content-Language", "")
                if not language:
                    soup = BeautifulSoup(response.content, "html.parser")
                    return soup.html.get("lang", "")
                return language
            return ""
        except Exception as e:
            files_mgmt.write_debug_line(f"Error getting website language: {e}")
            return ""

    def get_google_results_and_send_mails(self, lang: str, query: str):
        user_agent = get_random_user_agent(lang)
        ltd = get_ltd_from_lang(lang)
        nb_of_results = 0

        all_mails = []

        for result, url_base in enumerate(
            search(
                term=query,
                num_results=NB_DOMAINS_TO_CHECK,
                region=ltd,
                lang=ltd,
                # user_agent=user_agent,
            ),
            start=1,
        ):
            nb_of_results += 1
            files_mgmt.write_debug_line(f"Result {result}: {url_base}")
            url_base = self.normalize_url(url_base)

            if any(
                keyword.lower() in url_base.lower() for keyword in WEBSITES_TO_AVOID
            ) or (
                self.mongo_wrapper.get_mails_table_status(
                    url_base, lang, self.project, query
                )
            ):
                print("Website avoided : ", url_base)
                continue

            website_language = self.get_website_language(url_base)
            if not (lang in website_language.lower() or website_language == ""):
                continue

            time.sleep(random.uniform(0.1, 0.25))
            has_found_email = False

            slugs_to_check = SLUG_DICT[lang]

            for i, slug in enumerate(slugs_to_check):
                url = url_base + ("/" + slug if i > 0 else slug)
                try:
                    response = requests.get(url).text
                    response = self.bypass_email_protection(
                        self.remove_invalid_chars(response)
                    )
                    emails = self.get_unique_emails_from_html(response)
                    emails = self.filter_on_best_emails(emails)

                    if emails:
                        has_found_email = True

                        self.mongo_wrapper.add_mails_table_entry(
                            url_base, emails, True, self.project, lang, query
                        )

                        all_mails.extend(emails)

                        break

                    time.sleep(random.uniform(0.2, 0.5))
                except Exception as e:
                    if any(
                        err in str(e) for err in ["429", "403", "HTTPSConnectionPool"]
                    ):
                        break

            if not has_found_email:
                self.mongo_wrapper.add_mails_table_entry(
                    url_base, [], False, self.project, lang, query
                )
            time.sleep(random.uniform(0.2, 1))

        # For a lang, website and topic, send the same mails to all the recipients
        print(all_mails)
        self.mail_processor.send_emails(all_mails)

        return nb_of_results

    def run(self):
        files_mgmt.write_debug_line(
            "\n\n\n*********************************************\n****************** NEW RUN ******************\n*********************************************"
        )
        queries_file_path = os.path.join(BASE_APP_DIR, self.project, QUERIES_FILE_SLUG)
        queries = files_mgmt.populate_queries_from_text_file(queries_file_path)

        for query_json in queries:
            try:
                if query_json.get("query"):
                    query, lang, subject = (
                        query_json["query"],
                        query_json["lang"],
                        query_json["subject"],
                    )
                    self.mail_processor.set_lang_and_subject(lang, subject)

                    files_mgmt.write_debug_line(
                        f"\n****************** {self.project} ******************\n"
                    )

                    files_mgmt.write_debug_line(
                        json.dumps(query_json, indent=4, ensure_ascii=False)
                    )

                    self.mongo_wrapper.add_queries_table_entry(
                        self.project, lang, query
                    )

                    nb_of_results = self.get_google_results_and_send_mails(lang, query)

                    files_mgmt.write_debug_line(f"\n -->  {nb_of_results} results\n")

                    if nb_of_results >= MINIMAL_SATISFYING_NB_OF_RESULTS:
                        files_mgmt.pop_first_query_from_text_file(queries_file_path)
                    else:
                        self.mongo_wrapper.increment_queries_table_tried_number(
                            self.project, lang, query
                        )
                        if (
                            self.mongo_wrapper.get_queries_table_tried_number(
                                self.project, lang, query
                            )
                            >= 5
                        ):
                            files_mgmt.pop_first_query_from_text_file(queries_file_path)

                    files_mgmt.write_debug_line("\n--- EVERYTHING WENT WELL ---\n")

            except Exception as e:
                if DEBUG:
                    raise e
                else:
                    files_mgmt.write_debug_line(
                        f"\n--- FINISHED WITH ERROR : {e} ---\n"
                    )
