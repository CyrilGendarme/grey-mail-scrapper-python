import os
from config_and_texts.base_config import BASE_APP_DIR, CONFIG_FILE, PROJECTS_LIST
from misc.MongoWrapper import MongoWrapper
from submodules.config_manager.project_dynamic_config import ProjectConfig
from tasks.MailProcessor import MailProcessor, is_automatic_response


if __name__ == "__main__":
    for project in PROJECTS_LIST:
        config = ProjectConfig(
            os.path.join(BASE_APP_DIR, project, CONFIG_FILE),
            [
                "SMTP_SERVER",
                "SMTP_PORT",
                "EMAIL_ADDRESS",
                "EMAIL_PASSWORD",
                "DB_USER",
                "DB_PWD",
                "DB_TABLE_NAME",
                "DB_URI",
            ],
            {
                "SMTP_SERVER": "",
                "SMTP_PORT": 465,
                "EMAIL_ADDRESS": "",
                "EMAIL_PASSWORD": "",
                "DB_USER": "",
                "DB_PWD": "",
                "DB_TABLE_NAME": "",
                "DB_URI": "",
            },
        )
        mail_processor = MailProcessor(project, config)

        mongo_wrapper = MongoWrapper(config)
        mails = mongo_wrapper.get_all_mails_in_mails_timestamp_table()

        # Check and delete emails based on the condition
        mail_processor.check_and_delete_mails(mails, is_automatic_response)

        mongo_wrapper.clean_mails_from_mails_timestamp_table()
